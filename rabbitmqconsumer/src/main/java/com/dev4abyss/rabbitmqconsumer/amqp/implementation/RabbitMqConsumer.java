package com.dev4abyss.rabbitmqconsumer.amqp.implementation;

import com.dev4abyss.rabbitmqconsumer.amqp.AmqpConsumer;
import com.dev4abyss.rabbitmqconsumer.dto.Message;
import com.dev4abyss.rabbitmqconsumer.service.ConsumerServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class RabbitMqConsumer implements AmqpConsumer<Message> {


    private final ConsumerServiceImpl service;

    @Override
    @RabbitListener(queues = "${spring.rabbitmq.request.routing-key.producer}")
    public void consumer(Message message) throws Exception {
        try {
            service.action(message);
        } catch (Exception e) {
            throw new AmqpRejectAndDontRequeueException(e);
        }
    }
}
