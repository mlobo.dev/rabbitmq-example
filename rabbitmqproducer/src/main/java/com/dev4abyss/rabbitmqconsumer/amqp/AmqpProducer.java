package com.dev4abyss.rabbitmqconsumer.amqp;

public interface AmqpProducer<T> {

    void producer(T t);
}
