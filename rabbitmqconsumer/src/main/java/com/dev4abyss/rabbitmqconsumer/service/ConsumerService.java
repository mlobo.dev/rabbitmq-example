package com.dev4abyss.rabbitmqconsumer.service;

import com.dev4abyss.rabbitmqconsumer.dto.Message;

public interface ConsumerService {

    void action(Message message) throws Exception;
}
