package com.dev4abyss.rabbitmqconsumer.service;

import com.dev4abyss.rabbitmqconsumer.dto.Message;

public interface AmqpService {

    void sendToConsumer(Message message);
}
