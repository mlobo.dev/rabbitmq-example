package com.dev4abyss.rabbitmqconsumer.amqp;

public interface AmqpConsumer<T> {

    void consumer(T t) throws Exception;
}
