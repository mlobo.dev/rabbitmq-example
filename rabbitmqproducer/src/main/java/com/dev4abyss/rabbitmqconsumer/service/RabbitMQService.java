package com.dev4abyss.rabbitmqconsumer.service;

import com.dev4abyss.rabbitmqconsumer.amqp.AmqpProducer;
import com.dev4abyss.rabbitmqconsumer.dto.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class RabbitMQService implements AmqpService {


    private final AmqpProducer<Message> amqp;

    @Override
    public void sendToConsumer(Message message) {
        amqp.producer(message);
    }
}
