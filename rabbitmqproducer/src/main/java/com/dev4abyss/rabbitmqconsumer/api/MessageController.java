package com.dev4abyss.rabbitmqconsumer.api;

import com.dev4abyss.rabbitmqconsumer.dto.Message;
import com.dev4abyss.rabbitmqconsumer.service.AmqpService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/messages")
@Tag(name = "Message", description = "Messages")
public class MessageController {

    private final AmqpService service;

    @PostMapping("/send")
    public ResponseEntity<Void> sendToConsumer(@RequestBody Message message) {
        service.sendToConsumer(message);
        return ResponseEntity.ok().build();
    }

}