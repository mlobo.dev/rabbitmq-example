package com.dev4abyss.rabbitmqconsumer.service;

import com.dev4abyss.rabbitmqconsumer.dto.Message;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.stereotype.Service;

@Log4j2
@RequiredArgsConstructor
@Service
public class ConsumerServiceImpl implements ConsumerService {


    @Override
    public void action(Message message) throws Exception {
        if ("teste".equalsIgnoreCase(message.getText())) {
            throw new AmqpRejectAndDontRequeueException("error");
        }
        log.info(message.getText());
    }
}
