package com.dev4abyss.rabbitmqconsumer.dto;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class Message {

    private String text;
}
