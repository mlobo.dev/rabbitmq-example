package com.dev4abyss.rabbitmqconsumer.amqp.implementation;

import com.dev4abyss.rabbitmqconsumer.amqp.AmqpProducer;
import com.dev4abyss.rabbitmqconsumer.dto.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.AmqpRejectAndDontRequeueException;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ProducerRabbitMQ implements AmqpProducer<Message> {

    @Value("${spring.rabbitmq.request.routing-key.producer}")
    private String queue;

    @Value("${spring.rabbitmq.request.exchange.producer}")
    private String exchange;


    private final RabbitTemplate rabbitTemplate;


    @Override
    public void producer(Message message) {
        try {
            rabbitTemplate.convertAndSend(exchange, queue, message);
        } catch (Exception e) {
            throw new AmqpRejectAndDontRequeueException(e);
        }

    }
}
