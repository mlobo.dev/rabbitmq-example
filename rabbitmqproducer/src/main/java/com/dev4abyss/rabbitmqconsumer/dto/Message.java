package com.dev4abyss.rabbitmqconsumer.dto;

import lombok.Data;

@Data
public class Message {

    private String text;
}
